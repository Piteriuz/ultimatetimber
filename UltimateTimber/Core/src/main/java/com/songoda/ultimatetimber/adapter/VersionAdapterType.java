package com.songoda.ultimatetimber.adapter;

/**
 * The version adapter type
 * Used to determine what version adapter is being used
 */
public enum VersionAdapterType {
    CURRENT,
    LEGACY
}
